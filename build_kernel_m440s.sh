#!/bin/sh
export KERNELDIR=`readlink -f .`
export RAMFS_SOURCE=`readlink -f $KERNELDIR/ramdisk`
export USE_SEC_FIPS_MODE=true

echo "kerneldir = $KERNELDIR"
echo "ramfs_source = $RAMFS_SOURCE"

RAMFS_TMP="/tmp/arter97-ramdisk"

echo "ramfs_tmp = $RAMFS_TMP"

if [ "${1}" = "skip" ] ; then
	echo "Skipping Compilation"
	cd $KERNELDIR
else
	echo "Compiling kernel"
	cd $KERNELDIR
	cp defconfig .config
	scripts/configcleaner "
CONFIG_WLAN_REGION_CODE
CONFIG_TARGET_LOCALE_EUR
CONFIG_TARGET_LOCALE_USA
CONFIG_TARGET_LOCALE_KOR
CONFIG_MACH_M0_KOR_SKT
CONFIG_MACH_M0_KOR_KT
CONFIG_MACH_M0_KOR_LGT
"
	echo "
CONFIG_WLAN_REGION_CODE=201
# CONFIG_TARGET_LOCALE_EUR is not set
# CONFIG_TARGET_LOCALE_USA is not set
CONFIG_TARGET_LOCALE_KOR=y
CONFIG_MACH_M0_KOR_SKT=y
# CONFIG_MACH_M0_KOR_KT is not set
# CONFIG_MACH_M0_KOR_LGT is not set
" >> .config
	make || exit 1
fi

cp defconfig .config

echo "Building new ramdisk"
#remove previous ramfs files
rm -rf '$RAMFS_TMP'*
rm -rf $RAMFS_TMP
rm -rf $RAMFS_TMP.cpio.lz4
#copy ramfs files to tmp directory
cp -ax $RAMFS_SOURCE $RAMFS_TMP
cd $RAMFS_TMP
find . -name "*smdk4x12*" | while read file; do mv -f "$file" "$(echo $file | sed s/smdk4x12/SHW-M440S/g)"; done
find . -name "*rc*" | while read file; do sed -i -e s/smdk4x12/SHW-M440S/g $file ; done
find . -name "init" -exec chmod 755 {} \;
find . -name "*.SHW-M440S" -exec chmod 644 {} \;
find . -name "fstab*" -exec chmod 644 {} \;
find . -name "*.rc" -exec chmod 644 {} \;
find . -name "*.prop" -exec chmod 644 {} \;
find . -name "lib" -exec chmod -R 755 {} \;
#clear git repositories in ramfs
find . -name .git -exec rm -rf {} \;
find . -name EMPTY_DIRECTORY -exec rm -rf {} \;
cd $KERNELDIR
rm -rf $RAMFS_TMP/tmp/*
#remove mercurial repository
rm -rf $RAMFS_TMP/.hg

find . -name "*.ko" -exec cp {} . \;
ls *.ko | while read file; do /home/arter97/toolchain/bin/arm-eabi-strip --strip-unneeded $file ; done
cp -av *.ko $RAMFS_TMP/lib/modules/
chmod 644 $RAMFS_TMP/lib/modules/*
cd $RAMFS_TMP
find . | fakeroot cpio -H newc -o > $RAMFS_TMP.cpio
lz4 -c0 $RAMFS_TMP.cpio $RAMFS_TMP.cpio.lz4
ls -lh $RAMFS_TMP.cpio.lz4
# gzip -9 $RAMFS_TMP.cpio
cd $KERNELDIR

echo "Making new boot image"
./mkbootimg --kernel $KERNELDIR/arch/arm/boot/zImage --ramdisk $RAMFS_TMP.cpio.lz4 --board smdk4x12 --base 0x10000000 --pagesize 2048 --ramdiskaddr 0x11000000 -o $KERNELDIR/m440s.img

echo "done"
ls -al m440s.img
echo ""
ls -al *.ko
