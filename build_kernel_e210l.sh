#!/bin/sh
export KERNELDIR=`readlink -f .`
export RAMFS_SOURCE=`readlink -f $KERNELDIR/ramdisk`
export USE_SEC_FIPS_MODE=true

echo "kerneldir = $KERNELDIR"
echo "ramfs_source = $RAMFS_SOURCE"

RAMFS_TMP="/tmp/arter97-ramdisk"

echo "ramfs_tmp = $RAMFS_TMP"

cp defconfig .config
rm -rf include/generated
rm -rf include/config
rm -rf .version
make clean

scripts/configcleaner "
CONFIG_TARGET_LOCALE_USA
CONFIG_TARGET_LOCALE_KOR
CONFIG_MACH_C1_KOR_SKT
CONFIG_MACH_C1_KOR_KT
CONFIG_MACH_C1_KOR_LGT
CONFIG_MACH_M0
CONFIG_SEC_MODEM_M0
CONFIG_SEC_MODEM_C1
CONFIG_SEC_MODEM_C1_LGT
CONFIG_USBHUB_USB3503
CONFIG_UMTS_MODEM_XMM6262
CONFIG_LTE_MODEM_CMC221
CONFIG_IP_MULTICAST
CONFIG_LINK_DEVICE_DPRAM
CONFIG_LINK_DEVICE_USB
CONFIG_LINK_DEVICE_HSIC
CONFIG_IPC_CMC22x_OLD_RFS
CONFIG_SIPC_VER_5
CONFIG_SAMSUNG_MODULES
CONFIG_FM
CONFIG_FM_RADIO
CONFIG_FM_SI4709
CONFIG_FM_SI4705
CONFIG_WLAN_REGION_CODE
CONFIG_LTE_VIA_SWITCH
CONFIG_BRIDGE
CONFIG_FM34_WE395
CONFIG_CDMA_MODEM_CBP72
CONFIG_BRIDGE_NETFILTER
CONFIG_NETFILTER_XT_MATCH_PHYSDEV
CONFIG_BRIDGE_NF_EBTABLES
CONFIG_BRIDGE_IGMP_SNOOPING
"

echo "
# CONFIG_TARGET_LOCALE_USA is not set
CONFIG_TARGET_LOCALE_KOR=y
# CONFIG_MACH_C1_KOR_SKT is not set
# CONFIG_MACH_C1_KOR_KT is not set
CONFIG_MACH_C1_KOR_LGT=y
# CONFIG_MACH_M0 is not set
CONFIG_MACH_C1=y
# CONFIG_SEC_MODEM_M0 is not set
# CONFIG_SEC_MODEM_C1 is not set
CONFIG_SEC_MODEM_C1_LGT=y
CONFIG_MACH_NO_WESTBRIDGE=y
CONFIG_IP_MULTICAST=y
CONFIG_FM34_WE395=y
CONFIG_USBHUB_USB3503=y
# CONFIG_USBHUB_USB3503_OTG_CONN is not set
# CONFIG_UMTS_MODEM_XMM6262 is not set
CONFIG_LTE_MODEM_CMC221=y
CONFIG_LINK_DEVICE_DPRAM=y
CONFIG_LINK_DEVICE_USB=y
# CONFIG_LINK_DEVICE_HSIC is not set
CONFIG_IPC_CMC22x_OLD_RFS=y
CONFIG_SIPC_VER_5=y
# CONFIG_SAMSUNG_MODULES is not set
CONFIG_WLAN_REGION_CODE=203
CONFIG_LTE_VIA_SWITCH=y
CONFIG_BRIDGE=y
CONFIG_CDMA_MODEM_CBP72=y
# CONFIG_FM_RADIO is not set
# CONFIG_FM_SI4709 is not set
# CONFIG_FM_SI4705 is not set
CONFIG_BRIDGE_NETFILTER=y
# CONFIG_NETFILTER_XT_MATCH_PHYSDEV is not set
# CONFIG_BRIDGE_NF_EBTABLES is not set
# CONFIG_BRIDGE_IGMP_SNOOPING is not set
" >> .config

cd $KERNELDIR
make || exit 1
cp defconfig .config

echo "Building new ramdisk"
#remove previous ramfs files
rm -rf '$RAMFS_TMP'*
rm -rf $RAMFS_TMP
rm -rf $RAMFS_TMP.cpio.lz4
#copy ramfs files to tmp directory
cp -ax $RAMFS_SOURCE $RAMFS_TMP
rm $RAMFS_TMP/sbin/cbd
cp -p e210/cbd $RAMFS_TMP/sbin/
cd $RAMFS_TMP
find . -name "*smdk4x12*" | while read file; do mv -f "$file" "$(echo $file | sed s/smdk4x12/SHV-E210L/g)"; done
find . -name "*rc*" | while read file; do sed -i s/smdk4x12/SHV-E210L/g $file ; done
# so freaking-annoying HELL-G partition fix #1
find . -name "*rc*" | while read file; do sed -i -e s/mmcblk0p12/mmcblk0p13/g -e s/mmcblk0p11/mmcblk0p12/g -e s/mmcblk0p10/mmcblk0p11/g -e s/mmcblk0p9/mmcblk0p10/g -e s/mmcblk0p8/mmcblk0p9/g $file ; done
# so freaking-annoying HELL-G partition fix #2
find . -name "*fstab*" | while read file; do sed -i -e s/mmcblk0p12/mmcblk0p13/g -e s/mmcblk0p11/mmcblk0p12/g -e s/mmcblk0p10/mmcblk0p11/g -e s/mmcblk0p9/mmcblk0p10/g -e s/mmcblk0p8/mmcblk0p9/g $file ; done
# so freaking-annoying HELL-G communication-fix #1
sed -i 's/group radio cache inet misc audio sdcard_rw log//g' init.SHV-E210L.rc
# so freaking-annoying HELL-G communication-fix #2
sed -i '/service icd \/system\/bin\/icd/i\service cbd-lte \/sbin\/cbd -d -t cmc221 -b d -m d\n    class main\n    user root\n    group radio cache inet misc audio sdcard_rw log\n\nservice cbd-cdma \/sbin\/cbd -d -t cbp72 -b u -m d\n    class main\n    user root\n    group radio cache inet misc audio sdcard_rw log\n' init.SHV-E210L.rc
# so freaking-annoying HELL-G communication-fix #3
sed -i '/service cpboot-daemon \/sbin\/cbd -d/i\# RILD\nservice ril-daemon \/system\/bin\/rild\n    class main\n    socket rild stream 660 root radio\n    socket rild-debug stream 660 radio system\n    user root\n    group radio cache inet misc audio sdcard_rw log\n    onrestart restart cbd-lte\n    onrestart restart cbd-cdma' init.SHV-E210L.rc
# so freaking-annoying HELL-G communication-fix #4
perl -i -e 'undef $/; $_=<>; s/(service cpboot-daemon \/sbin\/cbd -d)(\s*)(    class main\n)(\s*)(    user root$)//gm; print' init.SHV-E210L.rc
# Thanks to gal3(?) in develoid
# LG: goto hell
chmod 644 *.rc
chmod 750 init*
chmod 640 fstab*
chmod 644 default.prop
chmod 771 data
chmod 755 dev
chmod 755 lib
chmod 755 lib/modules
chmod 644 lib/modules/*
chmod 755 proc
chmod 750 sbin
chmod 750 sbin/*
chmod 755 sys
chmod 755 system
#clear git repositories in ramfs
find . -name .git -exec rm -rf {} \;
find . -name EMPTY_DIRECTORY -exec rm -rf {} \;
cd $KERNELDIR
rm -rf $RAMFS_TMP/tmp/*
#remove mercurial repository
rm -rf $RAMFS_TMP/.hg

find . -name "*.ko" -exec cp {} . \;
ls *.ko | while read file; do /home/arter97/toolchain/bin/arm-eabi-strip --strip-unneeded $file ; done
cp -av *.ko $RAMFS_TMP/lib/modules/
chmod 644 $RAMFS_TMP/lib/modules/*
cd $RAMFS_TMP
find . | fakeroot cpio -H newc -o > $RAMFS_TMP.cpio
lz4 -c0 $RAMFS_TMP.cpio $RAMFS_TMP.cpio.lz4
ls -lh $RAMFS_TMP.cpio.lz4
# gzip -9 $RAMFS_TMP.cpio
cd $KERNELDIR

echo "Making new boot image"
./mkbootimg --kernel $KERNELDIR/arch/arm/boot/zImage --ramdisk $RAMFS_TMP.cpio.lz4 --board smdk4x12 --base 0x10000000 --pagesize 2048 --ramdiskaddr 0x11000000 -o $KERNELDIR/e210l.img

echo "done"
ls -al e210l.img
echo ""
ls -al *.ko
