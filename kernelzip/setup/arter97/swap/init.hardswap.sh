#!/system/bin/sh
#-----------------------------------------------------------------------
# Hard Swap activation by Yank555.lu v1.3
#-----------------------------------------------------------------------
# XDA thread : http://forum.xda-developers.com/showthread.php?t=1814311
#-----------------------------------------------------------------------
# Added dynamic swap partition lookup
#-----------------------------------------------------------------------
# Removed LMK settings
# Removed setting swappiness
# Removed specific busybox installation path (/system/xbin)
#-----------------------------------------------------------------------

logfile_location="/data"
sdcard_device="/dev/block/mmcblk1"
swap_partition=`fdisk -l $sdcard_device | grep swap | awk '{print $1;}'`
sdcard_wait=30

activate_swap () {

   echo "------- SD-card partition table ------" >>$logfile_location/swap.0.log;

   fdisk -l $sdcard_device >>$logfile_location/swap.0.log;
   
   echo "------- before activating swap -------" >>$logfile_location/swap.0.log;

   free >>$logfile_location/swap.0.log;
   echo "--------------------------------------" >>$logfile_location/swap.0.log;
   cat /proc/swaps >>$logfile_location/swap.0.log;

   echo "-------- activating swap now ---------" >>$logfile_location/swap.0.log;

   echo "  - swapon $swap_partition" >>$logfile_location/swap.0.log;
   swapon $swap_partition >>$logfile_location/swap.0.log;

   echo "------- after activating swap --------" >>$logfile_location/swap.0.log;

   free >>$logfile_location/swap.0.log;
   echo "--------------------------------------" >>$logfile_location/swap.0.log;
   cat /proc/swaps >>$logfile_location/swap.0.log;

   echo "--------------------------------------" >>$logfile_location/swap.0.log;

}

# Cycle Log files
rm $logfile_location/swap.4.log;
mv $logfile_location/swap.3.log $logfile_location/swap.4.log;
mv $logfile_location/swap.2.log $logfile_location/swap.3.log;
mv $logfile_location/swap.1.log $logfile_location/swap.2.log;
mv $logfile_location/swap.0.log $logfile_location/swap.1.log;

# Try to use swap partition
if [ "$swap_partition" != "" ]; # Check if a swap partition could be found
  then
    echo "SD-card swap partition has been found ... good !" >>$logfile_location/swap.0.log
    if [ -e $swap_partition ]; # Check if swap partition device exists
      then
        echo "SD-card swap partition device is present as well ... excellent !" >>$logfile_location/swap.0.log
        activate_swap;
      else
        echo "SD-card swap partition device not present yet ... waiting another $sdcard_wait seconds for SD-card to become ready ..." >>$logfile_location/swap.0.log
        sleep $sdcard_wait; # SD-card swap partition found, but no swap partition device yet, wait for slow card
        if [ -e $swap_partition ];
          then
            echo "SD-card swap partition device is present now ... eventually !" >>$logfile_location/swap.0.log;
            activate_swap;
          else
            echo "... still no swap partition device available after $sdcard_wait seconds, quitting ... sorry no swap for you, I'm afraid !" >>$logfile_location/swap.0.log;
        fi;
    fi;
  else
    echo "No SD-card swap partition has been found, quitting ... sorry no swap for you, I'm afraid !" >>$logfile_location/swap.0.log;
fi; 
