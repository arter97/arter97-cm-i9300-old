#!/sbin/sh
#----------------------------------------------
# Hard Swap v1.2 for SGS3 Flasher by Yank555.lu
#----------------------------------------------

log_file="/data/hardswap.log"
script_path="/tmp/arter97/swap/install"
feedback_file="$script_path/hardswap.prop"
sdcard_device="/dev/block/mmcblk1"
swap_partition=`fdisk -l $sdcard_device | grep swap | awk '{print $1;}'`

# Remove any prior logfiles / feedback files
rm $log_file
rm $feedback_file

echo "-----------------------------------------------" > $log_file
echo " Hard Swap Flasher by Yank555.lu v1.1 for SGS3 " >> $log_file
echo "-----------------------------------------------" >> $log_file
echo "SD-card device = $sdcard_device" >> $log_file
echo " " >> $log_file
echo "Paratition table for SD-card :" >> $log_file
echo "------------------------------" >> $log_file
fdisk -l $sdcard_device >> $log_file
echo " " >> $log_file
echo "Swap partition = $swap_partition" >> $log_file
echo " " >> $log_file

if [ "$swap_partition" = "" ];
  then

    echo "swap.present = no" >> $feedback_file

    echo "No swap partion present on SD-card ... aborting !" >> $log_file

  else

    echo "swap.present = yes" >> $feedback_file
    echo "swap.device = $swap_partition" >> $feedback_file

    echo "- formating swap partition '$swap_partition'" >> $log_file
    mkswap $swap_partition >> $log_file
    echo " " >> $log_file

    echo "swap.formated = yes" >> $feedback_file

    echo "- copying swap activation script to /system/etc" >> $log_file
    echo " " >> $log_file
    cp /tmp/arter97/swap/init.hardswap.sh /system/etc/init.hardswap.sh
    chmod 755 /system/etc/init.hardswap.sh
    chown 0:2000 /system/etc/init.hardswap.sh
    echo "done." >> $log_file

    echo "swap.scriptinstalled = yes" >> $feedback_file

fi;
