#!/sbin/sh

cd /data/data/

if [ ! -e /data/data/com.cyanogenmod.settings.device/shared_prefs/com.cyanogenmod.settings.device_preferences.xml ]; then
	rm -rf /data/data/com.cyanogenmod.settings.device
	tar -xf /tmp/arter97/script/com.cyanogenmod.settings.device.tar
else
	cat /data/data/com.cyanogenmod.settings.device/shared_prefs/com.cyanogenmod.settings.device_preferences.xml | grep -v mdnie_mode > mdnie_temp
	busybox sed -i -e 's#<map>#<map>\n<string name="mdnie_mode">1</string>#g' mdnie_temp
	mv mdnie_temp /data/data/com.cyanogenmod.settings.device/shared_prefs/com.cyanogenmod.settings.device_preferences.xml
	busybox chown system:system /data/data/com.cyanogenmod.settings.device/shared_prefs/com.cyanogenmod.settings.device_preferences.xml
	busybox chmod 660 /data/data/com.cyanogenmod.settings.device/shared_prefs/com.cyanogenmod.settings.device_preferences.xml
fi
