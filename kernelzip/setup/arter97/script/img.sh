#!/sbin/sh

cd /tmp/
/sbin/busybox unxz -c /tmp/img.tar.xz | tar -xf - "$@"
mv "$@" boot.img
