#!/sbin/sh

script_path="/tmp/arter97/script"
script_name="init.kernel.sh"
sub_scripts_path="$script_path/sub-scripts"
props_path="/tmp/aroma"

# Start making the script header -----------------------------------------------------------------

rm /data/kernel-script.log
rm /data/kernel-boot.log
cp $sub_scripts_path/header_1 $script_path/$script_name
cat $sub_scripts_path/header_2 >> $script_path/$script_name
echo "" >>$script_path/$script_name

# Add init.d configuration -----------------------------------------------------------------------

if [ "`grep selected.1= $props_path/initd.prop | awk -F= '{print $2;}'`" != "" ]
  then
    cat $sub_scripts_path/initd/1.`grep selected.1= $props_path/initd.prop | awk -F= '{print $2;}'` >>$script_path/$script_name
    if [ ! -e /system/etc/init.d ] ; then
        mkdir /system/etc/init.d
        chgrp 2000 /system/etc/init.d
        chmod 755 /system/etc/init.d
    fi
fi;

# Add CPU configuration --------------------------------------------------------------------------

if [ "`grep selected.1= $props_path/cpugov.prop | awk -F= '{print $2;}'`" != "" ]
  then
    cat $sub_scripts_path/cpugov/1.`grep selected.1= $props_path/cpugov.prop | awk -F= '{print $2;}'` >>$script_path/$script_name
fi;

# Add Frequency settings -------------------------------------------------------------------------

if [ "`grep selected.1= $props_path/cpugov.prop | awk -F= '{print $2;}'`" == "3" ]
  then
	if [ "`grep selected.1= $props_path/cpufreq.prop | awk -F= '{print $2;}'`" != "" ]
	  then
	    cat $sub_scripts_path/yankasus/1.`grep selected.1= $props_path/cpufreq.prop | awk -F= '{print $2;}'` >>$script_path/$script_name
	fi;
	if [ "`grep selected.2= $props_path/cpufreq.prop | awk -F= '{print $2;}'`" != "" ]
	  then
	    cat $sub_scripts_path/yankasus/2.`grep selected.2= $props_path/cpufreq.prop | awk -F= '{print $2;}'` >>$script_path/$script_name
	fi;
  else
	if [ "`grep selected.1= $props_path/cpufreq.prop | awk -F= '{print $2;}'`" != "" ]
	  then
	    cat $sub_scripts_path/cpufreq/1.`grep selected.1= $props_path/cpufreq.prop | awk -F= '{print $2;}'` >>$script_path/$script_name
	fi;
fi;

# Virtual memory configuration -------------------------------------------------------------------

if [ "`grep selected.2= $props_path/swap.prop | awk -F= '{print $2;}'`" != "" ]
  then
    cat $sub_scripts_path/swap/2.`grep selected.2= $props_path/swap.prop | awk -F= '{print $2;}'` >>$script_path/$script_name
fi;

if [ "`grep selected.1= $props_path/swap.prop | awk -F= '{print $2;}'`" != "" ]
  then
    cat $sub_scripts_path/swap/1.`grep selected.1= $props_path/swap.prop | awk -F= '{print $2;}'` >>$script_path/$script_name
fi;

if [ "`grep selected.3= $props_path/swap.prop | awk -F= '{print $2;}'`" != "" ]
  then
    cat $sub_scripts_path/swap/3.`grep selected.3= $props_path/swap.prop | awk -F= '{print $2;}'` >>$script_path/$script_name
fi;

# Fast charge configuration ----------------------------------------------------------------------

if [ "`grep selected.1= $props_path/charge.prop | awk -F= '{print $2;}'`" != "" ]
  then
    cat $sub_scripts_path/charge/1.`grep selected.1= $props_path/charge.prop | awk -F= '{print $2;}'` >>$script_path/$script_name
fi;

if [ "`grep selected.2= $props_path/charge.prop | awk -F= '{print $2;}'`" != "" ]
  then
    cat $sub_scripts_path/charge/2.`grep selected.2= $props_path/charge.prop | awk -F= '{print $2;}'` >>$script_path/$script_name
fi;

if [ "`grep selected.3= $props_path/charge.prop | awk -F= '{print $2;}'`" != "" ]
  then
    cat $sub_scripts_path/charge/3.`grep selected.3= $props_path/charge.prop | awk -F= '{print $2;}'` >>$script_path/$script_name
fi;

# Add fsync configuration ------------------------------------------------------------------------

if [ "`grep selected.1= $props_path/fsync.prop | awk -F= '{print $2;}'`" != "" ]
  then
    cat $sub_scripts_path/fsync/1.`grep selected.1= $props_path/fsync.prop | awk -F= '{print $2;}'` >>$script_path/$script_name
fi;

# Add kernel modules loading on boot -------------------------------------------------------------

if [ "`grep selected.1= $props_path/modules.prop | awk -F= '{print $2;}'`" != "" ]
  then
    cat $sub_scripts_path/modules/1.`grep selected.1= $props_path/modules.prop | awk -F= '{print $2;}'` >>$script_path/$script_name
fi;

if [ "`grep selected.2= $props_path/modules.prop | awk -F= '{print $2;}'`" != "" ]
  then
    cat $sub_scripts_path/modules/2.`grep selected.2= $props_path/modules.prop | awk -F= '{print $2;}'` >>$script_path/$script_name
fi;

if [ "`grep selected.3= $props_path/modules.prop | awk -F= '{print $2;}'`" != "" ]
  then
    cat $sub_scripts_path/modules/3.`grep selected.3= $props_path/modules.prop | awk -F= '{print $2;}'` >>$script_path/$script_name
fi;

if [ "`grep selected.4= $props_path/modules.prop | awk -F= '{print $2;}'`" != "" ]
  then
    cat $sub_scripts_path/modules/4.`grep selected.4= $props_path/modules.prop | awk -F= '{print $2;}'` >>$script_path/$script_name
fi;

if [ "`grep selected.5= $props_path/modules.prop | awk -F= '{print $2;}'`" != "" ]
  then
    cat $sub_scripts_path/modules/5.`grep selected.5= $props_path/modules.prop | awk -F= '{print $2;}'` >>$script_path/$script_name
fi;

if [ "`grep selected.6= $props_path/modules.prop | awk -F= '{print $2;}'`" != "" ]
  then
    cat $sub_scripts_path/modules/6.`grep selected.6= $props_path/modules.prop | awk -F= '{print $2;}'` >>$script_path/$script_name
fi;

# Add MMC I/O configuration ----------------------------------------------------------------------

if [ "`grep selected.1= $props_path/io.prop | awk -F= '{print $2;}'`" != "" ]
  then
    cat $sub_scripts_path/io/1.`grep selected.1= $props_path/io.prop | awk -F= '{print $2;}'` >>$script_path/$script_name
fi;

if [ "`grep selected.2= $props_path/io.prop | awk -F= '{print $2;}'`" != "" ]
  then
    cat $sub_scripts_path/io/2.`grep selected.2= $props_path/io.prop | awk -F= '{print $2;}'` >>$script_path/$script_name
fi;

# Add Android Low Memory Killer configuration ----------------------------------------------------

if [ "`grep selected.1= $props_path/lmk.prop | awk -F= '{print $2;}'`" != "" ]
  then
    cat $sub_scripts_path/lmk/1.`grep selected.1= $props_path/lmk.prop | awk -F= '{print $2;}'` >>$script_path/$script_name
fi;

# Boeffla Sound Engine ---------------------------------------------------------------------------

if [ "`grep selected.1= $props_path/boeffla.prop | awk -F= '{print $2;}'`" != "" ]
  then
    cat $sub_scripts_path/boeffla/1.`grep selected.1= $props_path/boeffla.prop | awk -F= '{print $2;}'` >>$script_path/$script_name
	if [ "`grep selected.1= $props_path/boeffla.prop | awk -F= '{print $2;}'`" != "3" ]
		then
			if [ "`grep selected.2= $props_path/boeffla.prop | awk -F= '{print $2;}'`" != "" ]
			  then
			    cat $sub_scripts_path/boeffla/2.`grep selected.2= $props_path/boeffla.prop | awk -F= '{print $2;}'` >>$script_path/$script_name
			fi;
			if [ "`grep selected.3= $props_path/boeffla.prop | awk -F= '{print $2;}'`" != "" ]
			  then
			    cat $sub_scripts_path/boeffla/3.`grep selected.3= $props_path/boeffla.prop | awk -F= '{print $2;}'` >>$script_path/$script_name
			fi;
			if [ "`grep selected.4= $props_path/boeffla.prop | awk -F= '{print $2;}'`" != "" ]
			  then
			    cat $sub_scripts_path/boeffla/4.`grep selected.4= $props_path/boeffla.prop | awk -F= '{print $2;}'` >>$script_path/$script_name
			fi;
			if [ "`grep selected.5= $props_path/boeffla.prop | awk -F= '{print $2;}'`" != "" ]
			  then
			    cat $sub_scripts_path/boeffla/5.`grep selected.5= $props_path/boeffla.prop | awk -F= '{print $2;}'` >>$script_path/$script_name
			fi;
			if [ "`grep selected.6= $props_path/boeffla.prop | awk -F= '{print $2;}'`" != "" ]
			  then
			    cat $sub_scripts_path/boeffla/6.`grep selected.6= $props_path/boeffla.prop | awk -F= '{print $2;}'` >>$script_path/$script_name
			fi;
			if [ "`grep selected.7= $props_path/boeffla.prop | awk -F= '{print $2;}'`" != "" ]
			  then
			    cat $sub_scripts_path/boeffla/7.`grep selected.7= $props_path/boeffla.prop | awk -F= '{print $2;}'` >>$script_path/$script_name
			fi;
			if [ "`grep selected.8= $props_path/boeffla.prop | awk -F= '{print $2;}'`" != "" ]
			  then
			    cat $sub_scripts_path/boeffla/8.`grep selected.8= $props_path/boeffla.prop | awk -F= '{print $2;}'` >>$script_path/$script_name
			fi;
			if [ "`grep selected.9= $props_path/boeffla.prop | awk -F= '{print $2;}'`" != "" ]
			  then
			    cat $sub_scripts_path/boeffla/9.`grep selected.9= $props_path/boeffla.prop | awk -F= '{print $2;}'` >>$script_path/$script_name
			fi;
			if [ "`grep selected.10= $props_path/boeffla.prop | awk -F= '{print $2;}'`" != "" ]
			  then
			    cat $sub_scripts_path/boeffla/10.`grep selected.10= $props_path/boeffla.prop | awk -F= '{print $2;}'` >>$script_path/$script_name
			fi;
	fi;
fi;

cat $sub_scripts_path/footer >>$script_path/$script_name

# Installing generated script to system init -----------------------------------------------------

rm /system/etc/$script_name
cp $script_path/$script_name /system/etc/$script_name
chmod 755 /system/etc/$script_name
chown 0:2000 /system/etc/$script_name

rm -rf /data/arter97-debug
