ui_print("@Preparing installation");
ui_print("@----------------------");
ui_print("");
ui_print(" - mounting system partition");
if file_getprop("/tmp/aroma/device.prop","selected.1") == "5"
  then
  assert(mount("ext4", "EMMC", "/dev/block/mmcblk0p10", "/system") || ui_print("   (system is mounted already)"));
else
  assert(mount("ext4", "EMMC", "/dev/block/mmcblk0p9", "/system") || ui_print("   (system is mounted already)"));
endif;
ui_print(" - mounting data partition");
if file_getprop("/tmp/aroma/device.prop","selected.1") == "5"
  then
  assert(mount("ext4", "EMMC", "/dev/block/mmcblk0p13", "/data") || ui_print("   (data is mounted already)"));
else
  assert(mount("ext4", "EMMC", "/dev/block/mmcblk0p12", "/data") || ui_print("   (data is mounted already)"));
endif;

ui_print(" - extracting installation files");
package_extract_dir("setup","/tmp");
set_perm_recursive(0, 0, 0777, 0777, "/tmp/arter97");

set_progress(0.1);

ui_print(" ");
ui_print("@Kernel installation");
ui_print("@-------------------");
ui_print(" ");

ui_print(" - Deleting old kernel modules");
run_program("/sbin/busybox", "rm", "-r", "/system/lib/modules");
ui_print(" - Installing new kernel modules");
symlink("/lib/modules", "/system/lib/modules");
ui_print(" - Installing hostapd hotspot fix");
delete("/system/bin/hostapd");
delete("/system/xbin/hostapd");
package_extract_dir("system", "/system");
set_perm(0, 0, 0775, "/system/bin/hostapd");
ui_print(" - Flashing new boot image");
run_program("/sbin/busybox", "dd", "if=/dev/zero", "of=/dev/block/mmcblk0p5");
set_progress(0.2);
package_extract_file("img.tar.xz", "/tmp/img.tar.xz");
set_progress(0.3);
if file_getprop("/tmp/aroma/device.prop","selected.1") == "1"
  then
  ui_print(" - Installing GT-I9300 boot.img");
  run_program("/tmp/arter97/script/img.sh", "i9300.img");
  write_raw_image("/tmp/boot.img", "/dev/block/mmcblk0p5");
  delete("/tmp/boot.img");
  run_program("/tmp/arter97/script/sub-scripts/wififix");
endif;
if file_getprop("/tmp/aroma/device.prop","selected.1") == "2"
  then
  ui_print(" - Installing SHW-M440S boot.img");
  run_program("/tmp/arter97/script/img.sh", "m440s.img");
  write_raw_image("/tmp/boot.img", "/dev/block/mmcblk0p5");
  delete("/tmp/boot.img");
  ui_print(" - Installing Wi-Fi murata fix");
  run_program("/tmp/arter97/script/sub-scripts/wififix");
endif;
if file_getprop("/tmp/aroma/device.prop","selected.1") == "3"
  then
  ui_print(" - Installing SHV-E210S boot.img");
  run_program("/tmp/arter97/script/img.sh", "e210s.img");
  write_raw_image("/tmp/boot.img", "/dev/block/mmcblk0p5");
  delete("/tmp/boot.img");
  ui_print(" - Installing Wi-Fi murata fix");
  run_program("/tmp/arter97/script/sub-scripts/wififix");
endif;
if file_getprop("/tmp/aroma/device.prop","selected.1") == "4"
  then
  ui_print(" - Installing SHV-E210K boot.img");
  run_program("/tmp/arter97/script/img.sh", "e210k.img");
  write_raw_image("/tmp/boot.img", "/dev/block/mmcblk0p5");
  delete("/tmp/boot.img");
  ui_print(" - Installing Wi-Fi murata fix");
  run_program("/tmp/arter97/script/sub-scripts/wififix");
endif;
if file_getprop("/tmp/aroma/device.prop","selected.1") == "5"
  then
  ui_print(" - Installing SHV-E210L boot.img");
  run_program("/tmp/arter97/script/img.sh", "e210l.img");
  write_raw_image("/tmp/boot.img", "/dev/block/mmcblk0p5");
  delete("/tmp/boot.img");
  ui_print(" - Installing Wi-Fi murata fix");
  run_program("/tmp/arter97/script/sub-scripts/wififix");
endif;
set_progress(0.4);
ui_print(" - Tuning file systems");
run_program("/sbin/tune2fs", "-o", "journal_data_writeback", "/dev/block/mmcblk0p3");
run_program("/sbin/tune2fs", "-o", "journal_data_writeback", "/dev/block/mmcblk0p8");
if file_getprop("/tmp/aroma/device.prop","selected.1") == "5"
  then
  run_program("/sbin/tune2fs", "-o", "journal_data_writeback", "/dev/block/mmcblk0p10");
  run_program("/sbin/tune2fs", "-o", "journal_data_writeback", "/dev/block/mmcblk0p13");
else
  run_program("/sbin/tune2fs", "-o", "journal_data_writeback", "/dev/block/mmcblk0p9");
  run_program("/sbin/tune2fs", "-o", "journal_data_writeback", "/dev/block/mmcblk0p12");
endif;
set_progress(0.45);

ui_print(" - Tweaking mDNIe");
run_program("/tmp/arter97/script/mdnie.sh");

set_progress(0.5);

ui_print(" ");
ui_print("@Script generation");
ui_print("@-----------------");
ui_print(" ");

ui_print(" - cleaning up /system/etc");
ui_print("   (kernel settings files)");
delete("/system/etc/init.kernel.sh");
delete("/system/etc/init.hardswap.sh");

ui_print(" - cleaning up /data");
ui_print("   (kernel old settings files)");
delete("/data/kernel-script.log");
delete("/data/hardswap.log");
delete("/data/swap.0.log");
delete("/data/swap.1.log");
delete("/data/swap.2.log");
delete("/data/swap.3.log");
delete("/data/swap.4.log");

ui_print(" - starting script generator");

run_program("/tmp/arter97/script/install.sh");

set_progress(0.7);

if file_getprop("/tmp/aroma/swap.prop","selected.1") == "2"
  then

    ui_print(" ");
    ui_print("@Hardswap installation");
    ui_print("@---------------------");
    ui_print(" ");

		ui_print(" - starting installation script");
    ui_print("   (see /data/hardswap.log for details)");
		run_program("/tmp/arter97/swap/install/install.sh");
		
		if file_getprop("/tmp/arter97/swap/install/hardswap.prop","swap.present") == "yes"
		  then
		    ui_print(" - swap partition found on SD-card");
		    ui_print("     '",file_getprop("/tmp/arter97/swap/install/hardswap.prop","swap.device"),"'");
		    if file_getprop("/tmp/arter97/swap/install/hardswap.prop","swap.formated") == "yes"
		      then
		        ui_print(" - swap partition formated");
		    endif;
		    if file_getprop("/tmp/arter97/swap/install/hardswap.prop","swap.scriptinstalled") == "yes"
		      then
		        ui_print(" - swap activation script installed to /system/etc");
		    endif;
		  else
		    ui_print("   Problem: no swap partiton found !");
		    ui_print("   --> Hardswap installation aborted.");
		endif;
  
endif;

set_progress(0.8);

ui_print(" ");
ui_print("@Cleaning up");
ui_print("@-----------");
ui_print(" ");

ui_print(" - wiping cache, dalvik-cache");
run_program("/sbin/busybox", "rm", "-r", "/data/dalvik-cache");
unmount("/cache");
if file_getprop("/tmp/aroma/device.prop","selected.1") == "5"
  then
  format("ext4", "EMMC", "/dev/block/mmcblk0p9");
else
  format("ext4", "EMMC", "/dev/block/mmcblk0p8");
endif;

set_progress(0.9);

ui_print(" - unmounting partition /data");
unmount("/data");

ui_print(" - unmounting partition /system");
unmount("/system");

ui_print(" - removing installation files");
delete_recursive(/tmp/arter97);

ui_print(" ");
ui_print("@done.");

set_progress(1.0);
