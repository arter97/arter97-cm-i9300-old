#!/bin/sh
make clean
rm -rf include/generated
rm -rf include/config
rm -rf .version
./build_kernel_i9300.sh
make clean
rm -rf include/generated
rm -rf include/config
rm -rf .version
./build_kernel_m440s.sh
./build_kernel_e210s.sh
./build_kernel_e210k.sh
./build_kernel_e210l.sh
rm arter97-kernel-"$(cat version)".zip 2>/dev/null
rm *.ko
cp *.img kernelzip/
cd kernelzip/
tar -cf - *.img | xz -9 -c - > img.tar.xz
rm *.img
7z a -mx9 arter97-kernel-"$(cat ../version)"-tmp.zip *
zipalign -v 4 arter97-kernel-"$(cat ../version)"-tmp.zip ../arter97-kernel-"$(cat ../version)".zip
rm arter97-kernel-"$(cat ../version)"-tmp.zip
rm img.tar.xz
cd ..
ls -al arter97-kernel-"$(cat version)".zip
